import React, { Component } from "react";
import { View, Image, TouchableOpacity, Text, StyleSheet } from "react-native";
import PropTypes from "prop-types";

import Back from "../resources/icons/close.png";

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    fontWeight: "400",
    color: "black",
    marginLeft: 24
  },
  titleContainer: {
    marginTop: 15,
    marginBottom: 15
  },
  info: {
    backgroundColor: "white",
    paddingTop: 15,
    paddingBottom: 15,
    marginTop: 2
  },
  infoText: {
    fontSize: 18,
    fontWeight: "400",
    color: "black",
    marginLeft: 24
  },
  chart: {
    flexDirection: "row",
    backgroundColor: "white",
    paddingTop: 15,
    paddingBottom: 15
  },
  chartTextTitle: {
    fontSize: 16,
    fontWeight: "400",
    width: "20%",
    paddingLeft: 24
  },
  chartText: {
    width: "80%",
    fontSize: 16,
    fontWeight: "400",
    color: "black"
  }
});

export default class UpcomingInfo extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.navigation.state.params.data
    };
  }

  back = () => {
    this.props.navigation.goBack();
  };

  render() {
    /* alert(JSON.stringify(this.state.data));*/
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>SQUAD </Text>
        </View>
        <View style={styles.info}>
          <Text style={styles.infoText}>{this.state.data.team_a}</Text>
        </View>
        <View style={styles.info}>
          <Text style={styles.infoText}>{this.state.data.team_b}</Text>
        </View>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>INFO </Text>
        </View>
        <View style={styles.chart}>
          <Text style={styles.chartTextTitle}>Match</Text>
          <Text style={styles.chartText}> : {this.state.data.format}</Text>
        </View>
        <View style={styles.chart}>
          <Text style={styles.chartTextTitle}>Series</Text>
          <Text style={styles.chartText}>: {this.state.data.season.name}</Text>
        </View>
        <View style={styles.chart}>
          <Text style={styles.chartTextTitle}>Date</Text>
          <Text style={styles.chartText}>: {this.state.data.start_date.str}</Text>
        </View>
        <View style={styles.chart}>
          <Text style={styles.chartTextTitle}>Venue</Text>
          <Text style={styles.chartText}>: {this.state.data.venue}</Text>
        </View>
      </View>
    );
  }
}
