import React from "react";
import { ScrollView, View } from "react-native";
import { Container, Tab, Tabs } from "native-base";
import PropTypes from "prop-types";
import GeneralHeader from "../components/HeaderGeneral";
import Tab1 from "../components/Latest";
import Tab2 from "../components/Catagories";
import Tab3 from "../components/Popular2";

const propTypes = {
  navigation: PropTypes.object.isRequired
};

const Products = props => (
  <ScrollView>
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <Container>
        <View>
          <GeneralHeader
            nav={props.navigation}
            leftIcon="ios-arrow-back"
            back="TigersShop"
            HeaderTittle="Products"
            RightPanelicon1="md-cart"
          />
        </View>
        <Tabs
          tabBarUnderlineStyle={{
            opacity: 0
          }}
          tabContainerStyle={{
            elevation: 0,
            height: 35,
            paddingLeft: "5%",
            backgroundColor: "white",
            paddingRight: "10%"
          }}
          initialPage={0}
        >
          <Tab
            activeTextStyle={{ fontSize: 12, color: "#4a4a4a" }}
            textStyle={{
              fontSize: 12,
              fontWeight: "normal",
              color: "#9b9b9b"
            }}
            activeTabStyle={{
              backgroundColor: "white",
              width: 30,
              borderBottomWidth: 2,
              borderBottomColor: "#23b06a"
            }}
            tabStyle={{
              backgroundColor: "white",
              width: 30,
              borderBottomWidth: 2,
              borderBottomColor: "white"
            }}
            heading="LATEST"
          >
            <Tab1 />
          </Tab>
          <Tab
            activeTextStyle={{ fontSize: 12, color: "#4a4a4a" }}
            textStyle={{
              fontSize: 12,
              fontWeight: "normal",
              color: "#9b9b9b"
            }}
            activeTabStyle={{
              backgroundColor: "white",
              width: 30,
              borderBottomWidth: 2,
              borderBottomColor: "#23b06a"
            }}
            tabStyle={{
              backgroundColor: "white",
              width: 30,
              borderBottomWidth: 2,
              borderBottomColor: "white"
            }}
            heading="CATEGORIES"
          >
            <Tab2 />
          </Tab>
          <Tab
            activeTextStyle={{ fontSize: 12, color: "#4a4a4a" }}
            textStyle={{
              fontSize: 12,
              fontWeight: "normal",
              color: "#9b9b9b"
            }}
            activeTabStyle={{
              backgroundColor: "white",
              width: 30,
              borderBottomWidth: 2,
              borderBottomColor: "#23b06a"
            }}
            tabStyle={{
              backgroundColor: "white",
              width: 30,
              borderBottomWidth: 2,
              borderBottomColor: "white"
            }}
            heading="POPULAR"
          >
            <Tab3 />
          </Tab>
        </Tabs>
      </Container>
    </View>
  </ScrollView>
);
Products.propTypes = propTypes;

export default Products;
