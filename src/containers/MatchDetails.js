import React, { Component } from "react";
import { View } from "react-native";
import PropTypes from "prop-types";
import { Container, Tab, Tabs } from "native-base";
import Tab1 from "../components/Summary";
import Tab2 from "../components/Scorecard";
import GeneralHeader from "../components/HeaderGeneral2";

export default class MatchDetails extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  componentWillMount() {
    // StatusBar.setHidden(false);
    // StatusBar.setBackgroundColor("#037d50");
  }
  render() {
    return (
      <Container>
        <View>
          <GeneralHeader
            nav={this.props.navigation}
            back="AllMatches"
            leftIcon="arrow-left"
            RightPanelicon1=""
            HeaderTittle="Match Details"
          />
        </View>
        <Tabs
          tabContainerStyle={{
            elevation: 1,
            height: 40,
            backgroundColor: "white",
            paddingLeft: "15%",
            paddingRight: "15%"
          }}
          tabBarUnderlineStyle={{ opacity: 0 }}
          initialPage={0}
        >
          <Tab
            activeTextStyle={{
              color: "rgb(94, 94, 94)",
              fontSize: 12,
              fontFamily: "Roboto"
            }}
            textStyle={{
              color: "#9b9b9b",
              fontSize: 12,
              fontFamily: "Roboto"
            }}
            activeTabStyle={{
              backgroundColor: "white",
              width: 30,
              borderBottomWidth: 3,
              borderBottomColor: "#23b06a"
            }}
            tabStyle={{
              backgroundColor: "white",
              width: 30,
              borderBottomWidth: 3,
              borderBottomColor: "white"
            }}
            heading="SUMMARY"
          >
            <Tab1 nav={this.props.navigation} />
          </Tab>
          <Tab
            activeTextStyle={{
              color: "rgb(94, 94, 94)",
              fontSize: 12,
              fontFamily: "Roboto"
            }}
            textStyle={{
              color: "#9b9b9b",
              fontSize: 12,
              fontFamily: "Roboto"
            }}
            activeTabStyle={{
              backgroundColor: "white",
              width: 30,
              borderBottomWidth: 3,
              borderBottomColor: "#23b06a"
            }}
            tabStyle={{
              backgroundColor: "white",
              width: 30,
              borderBottomWidth: 3,
              borderBottomColor: "white"
            }}
            heading="SCORECARD"
          >
            <Tab2 nav={this.props.navigation} />
          </Tab>
        </Tabs>
        <View />
      </Container>
    );
  }
}
