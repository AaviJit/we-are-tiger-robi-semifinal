import React, { Component } from "react";
import { View } from "react-native";

export default class Comment extends Component {
  componentWillMount() {}
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View
          style={{
            height: 2,
            width: "100%",
            backgroundColor: "grey",
            marginBottom: 10
          }}
        />
        <View style={{ flexDirection: "row" }} />
        <View
          style={{
            height: 2,
            width: "100%",
            backgroundColor: "grey",
            marginTop: 10
          }}
        />
      </View>
    );
  }
}
