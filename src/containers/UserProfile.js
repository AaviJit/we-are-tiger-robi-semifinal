/* eslint-disable no-console*/
import React, { Component } from "react";
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  AsyncStorage,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import axios from "axios";
import ImagePicker from "react-native-image-crop-picker";
import ActionSheet from "react-native-actionsheet";
import PropTypes from "prop-types";
import { Button } from "native-base";
import { Switch } from "react-native-switch";
import Customheader from "../components/HeaderGeneral2";
import profilepic from "../resources/img/default-pic.png";
import { DrawerChecker, DrawerChanger } from "../components/DrawerChecker";

const styles = StyleSheet.create({
  label: {
    width: "100%",
    height: "6%",
    backgroundColor: "#f4f4f4",
    flexDirection: "column",
    justifyContent: "center",
    paddingHorizontal: "5%"
  },
  division: {
    width: "100%",
    height: "6.3%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    // alignItems: "center",
    paddingHorizontal: "8%",
    borderBottomWidth: 1,
    borderBottomColor: "#f1f1f1",
    paddingTop: "2%"
  },
  divisionText: {
    fontFamily: "Roboto-Regular",
    fontSize: 15
  },
  footings: {
    width: "100%",
    height: "9%",
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: "8%"
  }
});

export default class UserProfile extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      name: "Name",
      bio: "Bio",
      phone: "Phone No",
      gender: "Gender",
      username: "@username",
      website: "Website",
      loading: true
    };
  }

  componentWillMount() {
    AsyncStorage.getItem("user").then(profile => {
      const x = JSON.parse(profile);
      axios
        .get(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/user/?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret`
        )
        .then(user => {
          console.log(user);
          this.setState(
            {
              email: user.data.email,
              name: user.data.lastName,
              bio: "Bio",
              phone: user.data.phoneNumber,
              gender: "Gender",
              username: user.data.firstName,
              website: "Website",
              profilePic: user.data.profilePicture.url
            },
            () => {
              this.setState({
                loading: false
              });
            }
          );
        })
        .catch(err => {
          console.log(err);
          this.setState({
            loading: false
          });
        });
    });
    const drawerOpen = DrawerChecker("drawer");
    if (drawerOpen) {
      DrawerChanger(false);
      return true;
    }
  }

  getImage = index => {
    switch (index) {
      case 0:
        ImagePicker.openCamera({
          width: 300,
          height: 300,
          cropping: true
        }).then(image => {
          this.sendImage(image);
        });
        break;
      case 1:
        ImagePicker.openPicker({
          width: 300,
          height: 300,
          cropping: true
        }).then(image => {
          this.sendImage(image);
        });
        break;
      default:
    }
  };

  sendImage = image => {
    AsyncStorage.getItem("user").then(profile => {
      const x = JSON.parse(profile);
      axios
        .get(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/user/upload/profile-picture?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret&imageId=1`,
          image.path,
          {
            headers: {
              "Content-Type": image.mime
            }
          }
        )
        .then(response => {
          console.log(response);
        })
        .catch(error => {
          console.log(error);
        });
    });
  };

  openActionSheet = () => {
    this.ActionSheet.show();
  };

  render() {
    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: "white",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <ActivityIndicator size="large" color="#rgb(54,190,107)" />
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <Customheader
          leftIcon="arrow-left"
          HeaderTittle="User Profile"
          nav={this.props.navigation}
          back="HomePage"
          RightPanelIcon1=""
        />
        <ScrollView>
          <View style={{ height: 770 }}>
            <View
              style={{
                width: "100%",
                height: "18%",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "white"
              }}
            >
              <TouchableOpacity onPress={this.openActionSheet}>
                <ActionSheet
                  ref={o => (this.ActionSheet = o)}
                  title="Choose Source"
                  options={["Take Picture", "Select from Library", "Cancel"]}
                  cancelButtonIndex={2}
                  onPress={index => {
                    this.getImage(index);
                  }}
                />
                {this.state.profilePic ? (
                  <Image
                    style={{
                      height: 120,
                      width: 120,
                      borderRadius: 60
                    }}
                    source={{
                      uri: `http://206.189.159.149:8080/abc/${
                        this.state.profilePic
                      }`
                    }}
                  />
                ) : (
                  <Image
                    style={{
                      height: 120,
                      width: 120,
                      borderRadius: 60
                    }}
                    source={require("../resources/img/default-pic.png")}
                  />
                )}
              </TouchableOpacity>
            </View>
            <View style={styles.division}>
              <Text style={styles.divisionText}>{this.state.name} </Text>
            </View>
            <View style={styles.division}>
              <Text style={styles.divisionText}>{this.state.username} </Text>
            </View>
            <View style={styles.division}>
              <Text style={styles.divisionText}>{this.state.website} </Text>
            </View>
            <View style={styles.division}>
              <Text style={styles.divisionText}>{this.state.bio} </Text>
            </View>
            <View style={styles.label}>
              <Text
                style={{
                  fontFamily: "Roboto-Black",
                  // lineHeight: 15,
                  fontSize: 12,
                  color: "#4a4a4a",
                  fontWeight: "bold"
                }}
              >
                PERSONAL INFORMATION
              </Text>
            </View>
            <View style={styles.division}>
              <Text style={styles.divisionText}>{this.state.email} </Text>
            </View>
            <View style={styles.division}>
              <Text style={styles.divisionText}>{this.state.phone} </Text>
            </View>
            <View style={styles.division}>
              <Text style={styles.divisionText}>{this.state.gender} </Text>
            </View>
            <View style={styles.label}>
              <Text
                style={{
                  fontFamily: "Roboto-Black",
                  // lineHeight: 15,
                  fontSize: 12,
                  color: "#4a4a4a",
                  fontWeight: "bold"
                }}
              >
                CONNECT YOUR ACCOUNT
              </Text>
            </View>
            <View style={styles.division}>
              <Text style={styles.divisionText}>Facebook</Text>
              <Switch
                value
                onValueChange={val => console.log(val)}
                disabled={false}
                circleSize={26}
                barHeight={30}
                circleBorderWidth={0}
                backgroundActive="#23b06a"
                backgroundInactive="gray"
                circleActiveColor="white"
                circleInActiveColor="white"
                changeValueImmediately
                // custom component to render inside the Switch circle (Text, Image, etc.)
                // if rendering inside circle, change state immediately or wait for animation to complete
                innerCircleStyle={{
                  alignItems: "center",
                  justifyContent: "center"
                }} // style for inner animated circle for what you (may) be rendering inside the circle
                outerCircleStyle={{}} // style for outer animated circle
                renderActiveText={false}
                renderInActiveText={false}
                switchLeftPx={3} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
                switchRightPx={3} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
                switchWidthMultiplier={1.9}
              />
            </View>

            <View
              style={{
                width: "100%",
                flexDirection: "column",
                backgroundColor: "#ffffff",
                justifyContent: "center",
                alignContent: "center",
                padding: 14,
                paddingBottom: 30
              }}
            >
              <View
                style={{
                  marginTop: 20,
                  width: "95%",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "white",
                  left: "2%"
                }}
                rounded
                success
              >
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: "Roboto",
                    fontWeight: "bold",
                    color: "#d0021b"
                  }}
                >
                  Delete account
                </Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
