import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  Image,
  AsyncStorage
} from "react-native";
import Ad from "../resources/img/ad.png";
import axios from "axios";

// import PropTypes from 'prop-types';
const styles = StyleSheet.create({
  Title: {
    paddingTop: 10,
    alignItems: "center",
    paddingBottom: 5
  },
  TitleText: {
    color: "#1f1f1f",
    fontFamily: "Roboto",
    fontSize: 15,
    fontWeight: "300",
    alignItems: "center",
    justifyContent: "center"
  },
  BoxTitleLeft: {
    marginTop: 5,
    marginLeft: 11,
    fontSize: 10,
    fontFamily: "Roboto",
    color: "#1f1f1f",
    fontWeight: "normal",
    justifyContent: "center"
  },
  BoxTitleRight: {
    marginTop: 5,
    marginRight: 148,
    fontSize: 11,
    fontFamily: "Roboto",
    color: "#1f1f1f",
    fontWeight: "normal"
  },
  ContentTextLeft: {
    marginTop: 10,
    marginLeft: 20,
    fontSize: 8,
    fontFamily: "Roboto",
    color: "#1f1f1f",
    fontWeight: "normal"
  },
  ContentTextLeftGrey: {
    marginTop: 5,
    fontSize: 8,
    fontFamily: "Roboto",
    color: "#1f1f1f",
    fontWeight: "normal"
  },
  ContentTextRightTopGrey: {
    marginTop: 10,
    marginLeft: 30,
    marginRight: 130,
    fontSize: 8,
    fontFamily: "Roboto",
    color: "grey",
    fontWeight: "normal"
  },
  ContentTextRightmiddle: {
    marginTop: 5,
    marginLeft: 30,
    marginRight: 130,
    fontSize: 8,
    fontFamily: "Roboto",
    color: "#1f1f1f",
    fontWeight: "normal"
  },
  ContentTextRightBottomGrey: {
    marginTop: 5,
    marginLeft: 30,
    marginRight: 130,
    fontSize: 8,
    fontFamily: "Roboto",
    color: "grey",
    fontWeight: "normal"
  }
});
export default class Schedule extends Component {
  constructor(props) {
    super(props);
    this.state = {
      schedules: [],
      topBanner: ""
    };
  }

  componentWillMount() {
    AsyncStorage.getItem("user").then(profile => {
      const x = JSON.parse(profile);
      axios
        .get(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/banner/specific?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret&pageNumber=10&position=1`
        )
        .then(banner => {
          console.log(banner);
          this.setState({
            topBanner: banner.data.image.url
          });
        });
      axios
        .get(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/schedules?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret&page=0`
        )
        .then(schedules => {
          schedules.data.content.map(x => {
            const date = new Date(x.matchTime * 1000);
            //Get day
            const day = date.getDay();
            // Hours part from the timestamp
            const hours = date.getHours();
            // Minutes part from the timestamp
            const minutes = `0${date.getMinutes()}`;
            // Seconds part from the timestamp
            const seconds = `0${date.getSeconds()}`;
            const formattedDay = `${date.getDate()}/${date.getMonth() +
              1}/${date.getFullYear()}`;
            // Will display time in 10:30:23 format
            const formattedTime = `${hours}:${minutes.substr(-2)}`;
            x.time = formattedTime;
            x.day = formattedDay;
            console.log(x);
            this.setState({
              schedules: [...this.state.schedules, x]
            });
            return false;
          });
          console.log(this.state.schedules);
        });
    });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView>
          <View style={{ flex: 1 }}>
            <View style={styles.Title}>
              <Text style={styles.TitleText}>
                Bangladesh year long schedule 2018
              </Text>
            </View>
            <View
              style={{
                height: 1,
                width: "100%",
                backgroundColor: "#e9ebee",
                marginTop: 10
              }}
            />
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <Text style={styles.BoxTitleLeft}>DATE AND TIME</Text>
              <Text style={styles.BoxTitleRight}>MATCH</Text>
            </View>
            {this.state.schedules.map(schedules => (
              <View>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between"
                  }}
                >
                  <View>
                    <View style={{ flexDirection: "row" }}>
                      <Text style={styles.ContentTextLeft}>
                        {schedules.day}
                      </Text>
                      <Text style={styles.ContentTextLeftGrey} />
                    </View>
                    <View style={{ flexDirection: "row" }}>
                      <Text style={styles.ContentTextLeft}>
                        {schedules.time}
                      </Text>
                      <Text style={styles.ContentTextLeftGrey} />
                    </View>
                  </View>
                  <View>
                    <Text style={styles.ContentTextRightTopGrey}>
                      {schedules.seriesName}
                    </Text>
                    <Text style={styles.ContentTextRightmiddle}>
                      {schedules.team1} VS {schedules.team2}
                    </Text>
                    <Text style={styles.ContentTextRightBottomGrey}>
                      {schedules.venue}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "#e9ebee",
                    marginTop: 10,
                    marginBottom: 10
                  }}
                />
              </View>
            ))}
          </View>
        </ScrollView>
        {this.state.topBanner !== "" ? (
          <View
            style={{
              width: "100%",
              backgroundColor: "white",
              // flex: 1,
              paddingBottom: 70,
              paddingTop: 2,
              // paddingLeft: 15,
              // paddingRight: 15,
              flexDirection: "row",
              justifyContent: "center"
            }}
          >
            <Image
              style={{ width: "80%", height: 60, resizeMode: "cover" }}
              source={{
                uri: `http://206.189.159.149:8080/abc/${this.state.topBanner}`
              }}
            />
          </View>
        ) : null}
      </View>
    );
  }
}
