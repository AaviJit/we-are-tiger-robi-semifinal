import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  Image,
  ListView,
  StyleSheet,
  View,
  AsyncStorage,
  Alert,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import axios from "axios";
import InfiniteScroll from "react-native-infinite-scroll";
import { getuserdetails } from "../components/authorizationandrefreshtoken";

const styles = StyleSheet.create({
  container: {
    // paddingBottom: 60,
    flex: 1
    //backgroundColor: "orange"
  },
  carditem: {
    width: "100%",
    backgroundColor: "white",
    // flex: 1,
    paddingBottom: 2,
    paddingTop: 2,
    // paddingLeft: 15,
    // paddingRight: 15,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  cardImage: {
    height: 200,
    width: "100%"
    //backgroundColor: "blue"
  },
  cardImageVideos: {
    height: 100,
    width: 150
  },
  newsCategory: {
    backgroundColor: "#169757",
    color: "white",
    width: 70,
    fontFamily: "Roboto",
    // textAlign:'left',
    fontSize: 12,
    textAlign: "center"
  },
  newsTitle: {
    paddingTop: 2,
    color: "white",
    fontFamily: "roboto",
    fontSize: 20,
    fontWeight: "normal"
    // backgroundColor:'red'
  },
  newsTime: {
    paddingTop: 5,
    // color: 'white',
    fontFamily: "roboto",
    fontSize: 12,
    fontWeight: "normal",
    textAlign: "left",
    color: "rgba(255, 255, 255, 0.87)"
  },
  cardContent: {
    flexDirection: "column",
    justifyContent: "flex-end",
    height: "100%",
    // backgroundColor:'grey',
    marginHorizontal: 12,
    paddingVertical: 5
  },

  exploremore: {
    width: "100%",
    backgroundColor: "white",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderTopColor: "#ffffff",
    borderBottomColor: "#ffffff"
  },
  textandvideolength: {
    flexDirection: "column",
    flexWrap: "wrap"
  }
});

export default class NewsTab extends Component {
  static propTypes = {
    nav: PropTypes.object.isRequired
  };
  // pagination_no = 0;
  constructor(props) {
    super(props);
    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      pageNo: 0,
      access_token: "",
      newsObjects: [],
      loading: true,
      topBanner: "",
      bottomBanner: ""
    };
  }

  componentDidMount() {
    AsyncStorage.getItem("user")
      .then(response => {
        //alert(JSON.stringify(response))
        const x = JSON.parse(response);
        this.setState(
          {
            access_token: x.access_token
          },
          () => {
            this.axiosGetVideoContents(
              `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/news/page?access_token=${
                x.access_token
              }&client_id=android-client&client_secret=android-secret&id=0`
            );
            axios
              .get(
                `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/banner/specific?access_token=${
                  x.access_token
                }&client_id=android-client&client_secret=android-secret&pageNumber=5&position=1`
              )
              .then(banner => {
                console.log(banner);
                this.setState({
                  topBanner: banner.data.image.url
                });
              });
          }
        );
      })
      .catch(() => {
        Alert.alert(
          "Cannot connect to internal storage, make sure you have the correct storage rights."
        );
      });
  }
  loadMorePage = () => {
    this.setState({
      loading: false
    });
    this.setState({
      pageNo: this.state.pageNo + 1
    });
    this.axiosGetVideoContents(
      `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/news/page?access_token=${
        this.state.access_token
      }&client_id=android-client&client_secret=android-secret&id=${
        this.state.pageNo
      }`
    );
  };

  navigatetonewsdetails = id => {
    this.props.nav.navigate("NewsDetails", { newsid: id });
  };

  axiosGetVideoContents = async urlvariable => {
    axios
      .get(urlvariable)
      .then(response => {
        this.setState({
          loading: false
        });
        //console.log(response.data.content[0].featruedImage.url);
        response.data.content.map(news => {
          this.setState({
            newsObjects: [...this.state.newsObjects, news]
          });
          return "";
        });
        // alert(JSON.stringify(this.state.videoObjects));
      })
      .catch(error => {
        //console.log(error);
        if (error.response.status === 401) {
          getuserdetails()
            .then(res => {
              this.setState(
                {
                  access_token: res.access_token
                },
                () => {
                  this.axiosGetVideoContents(
                    `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/news/page?access_token=${
                      this.state.access_token
                    }&client_id=android-client&client_secret=android-secret&id=${
                      this.state.pageNo
                    }`
                  );
                }
              );
            })
            .catch(() => {
              this.setState({
                loading: false
              });
              Alert.alert(
                "you are being logged out for unavilability, Please log in again!"
              );
              this.props.nav.navigate("LoginPage");
            });
        } else {
          this.setState({
            loading: false
          });
        }
      });
  };

  render() {
    const rows = this.dataSource.cloneWithRows(this.state.newsObjects || []);
    return (
      <View style={styles.container}>
        {this.state.topBanner !== "" ? (
          <View
            style={{
              width: "100%",
              backgroundColor: "white",
              // flex: 1,
              paddingBottom: 2,
              paddingTop: 2,
              // paddingLeft: 15,
              // paddingRight: 15,
              flexDirection: "row",
              justifyContent: "center"
            }}
          >
            <Image
              style={{ width: "80%", height: 60, resizeMode: "cover" }}
              source={{
                uri: `http://206.189.159.149:8080/abc/${this.state.topBanner}`
              }}
            />
          </View>
        ) : null}
        <InfiniteScroll
          horizontal={false} //true - if you want in horizontal
          onLoadMoreAsync={this.loadMorePage}
          distanceFromEnd={1}
        >
          <View style={styles.news}>
            <ListView
              enableEmptySections
              dataSource={rows}
              renderRow={data => (
                <TouchableOpacity
                  onPress={() => this.navigatetonewsdetails(data.id)}
                >
                  <View style={styles.carditem}>
                    <Image
                      style={styles.cardImage}
                      source={{
                        uri: `http://206.189.159.149:8080/abc/${
                          data.featruedImage.url
                        }`
                      }}
                    >
                      <View style={styles.cardContent}>
                        <Text style={styles.newsCategory}>CATEGORY</Text>
                        <Text style={styles.newsTitle}>{data.title}</Text>
                        <Text style={styles.newsTime}>{data.createdAt}</Text>
                      </View>
                    </Image>
                  </View>
                </TouchableOpacity>
              )}
            />
            {this.state.loading && (
              <ActivityIndicator size="large" color="#rgb(54,190,107)" />
            )}
          </View>
        </InfiniteScroll>
        {this.state.bottomBanner !== "" ?  <View
          style={{
            width: "100%",
            backgroundColor: "white",
            // flex: 1,
            paddingTop: 2,
            paddingBottom: 60,
            // paddingLeft: 15,
            // paddingRight: 15,
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          <Image
            style={{ width: "80%", height: 60, resizeMode: "cover" }}
            source={{
              uri: `http://206.189.159.149:8080/abc/${this.state.bottomBanner}`
            }}
          />
        </View>: 
      null}
       
      </View>
    );
  }
}
