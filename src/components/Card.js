/* eslint-disable no-console*/
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
  AsyncStorage,
  WebView
} from "react-native";
import Thumbnail from "react-native-thumbnail-video";
import axios from "axios";
import CarouselPager from "react-native-carousel-pager";
import HasanPic from "../resources/icons/hasan.jpg";
import RobiAd from "../resources/icons/robiradd.png";

const styles = StyleSheet.create({
  container: {
    paddingBottom: 60,
    flex: 1
  },
  carditem: {
    width: "100%",
    backgroundColor: "white",
    // flex: 1,
    paddingBottom: 2,
    paddingTop: 2,
    // paddingLeft: 15,
    // paddingRight: 15,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  cardImage: {
    height: 200,
    width: "100%"
  },
  cardImageVideos: {
    height: 100,
    width: 150
  },
  newsCategory: {
    backgroundColor: "#169757",
    color: "white",
    width: 70,
    fontFamily: "Roboto",
    // textAlign:'left',
    fontSize: 12,
    textAlign: "center"
  },
  newsTitle: {
    paddingTop: 2,
    color: "white",
    fontFamily: "roboto",
    fontSize: 20,
    fontWeight: "normal",
    backgroundColor: "transparent"
    // backgroundColor:'red'
  },
  newsTime: {
    paddingTop: 5,
    // color: 'white',
    fontFamily: "roboto",
    fontSize: 12,
    fontWeight: "normal",
    textAlign: "left",
    color: "rgba(255, 255, 255, 0.87)",
    backgroundColor: "transparent"
  },
  cardContent: {
    flexDirection: "column",
    justifyContent: "flex-end",
    height: "100%",
    // backgroundColor:'grey',
    marginHorizontal: 12,
    paddingVertical: 5
  },
  exploremore: {
    width: "100%",
    backgroundColor: "white",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderTopColor: "#ffffff",
    borderBottomColor: "#ffffff"
  },
  textandvideolength: {
    flexDirection: "column",
    flexWrap: "wrap"
  }
});

export default class Cardrender extends Component {
  static propTypes = {
    nav: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      myObjects: [],
      topUpdates: [],
      topBanner: "",
      bottomBanner: "",
      axiosvideo:
        "http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/video/page?access_token=da5b84ef-714f-4814-a962-0c7dfa95d76e&client_id=android-client&client_secret=android-secret&id=0",
      axiosnews:
        "http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/news/page?access_token=da5b84ef-714f-4814-a962-0c7dfa95d76e&client_id=android-client&client_secret=android-secret&id=0"
    };
  }

  componentWillMount() {
    AsyncStorage.getItem("user").then(response => {
      const x = JSON.parse(response);
      axios
        .get(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/topUpdates/?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret`
        )
        .then(topUpdates => {
          topUpdates.data.content.map(topUpdate => {
            this.setState({
              topUpdates: [...this.state.topUpdates, topUpdate]
            });
            return null;
          });
        });
      axios
        .get(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/triviaCorner/?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret&liveAt=2018-05-20-00:00:00`
        )
        .then(trivia => {
          console.log("trivia")
         console.log(trivia);
        });
      axios
        .get(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/banner/specific?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret&pageNumber=1&position=1`
        )
        .then(banner => {
          console.log(banner);
          this.setState({
            topBanner: banner.data.image.url
          });
        });
      axios
        .get(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/banner/specific?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret&pageNumber=1&position=2`
        )
        .then(banner => {
          this.setState({
            bottomBanner: banner.data.image.url
          });
        });
    });

    this.axiosGetContents(this.state.axiosnews);
    this.axiosGetContents(this.state.axiosvideo);
    this.axiosGetContents(this.state.axiosbanner);
    this.axiosGetContents(this.state.axiosnews);
  }
  componentDidMount() {
    // alert(JSON.stringify(this.state.myObjects));
  }

  onClickSomething() {
    this.carousel.goToPage(2);
  }

  navigatetonewsdetails = id => {
    this.props.nav.navigate("NewsDetails", { newsid: id });
  };

  toNewsAndVideos = () => {
    this.props.nav.navigate("NewsAndVideos");
  };

  axiosGetContents = async urlvariable => {
    axios
      .get(urlvariable)
      .then(response => {
        // alert('functioning'+ urlvariable);
        response.data.map(x => {
          this.setState({
            myObjects: [...this.state.myObjects, x]
          });
          return false;
        });
      })
      .catch(error => {
        //alert('Sorry, no More videos to display!');
        console.log(error);
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.news}>
          {this.state.topUpdates.map(
            (topUpdate, index) =>
              topUpdate.video === null ? (
                <View>
                  {index <= 1 ? (
                    <View>
                      <TouchableOpacity
                        onPress={() =>
                          this.navigatetonewsdetails(topUpdate.news.id)
                        }
                      >
                        <View style={styles.carditem}>
                          <Image
                            style={styles.cardImage}
                            source={{
                              uri: `http://206.189.159.149:8080/abc/${
                                topUpdate.news.featruedImage.url
                              }`
                            }}
                          >
                            <View style={styles.cardContent}>
                              <Text style={styles.newsCategory}>CATEGORY</Text>
                              <Text style={styles.newsTitle}>
                                {topUpdate.news.title}
                              </Text>
                              <Text style={styles.newsTime}>
                                {topUpdate.news.createdAt}
                              </Text>
                            </View>
                          </Image>
                        </View>
                      </TouchableOpacity>
                      <View
                        style={{
                          justifyContent: "center",
                          alignItems: "center",
                          paddingTop: 10,
                          backgroundColor: "white"
                        }}
                      >
                        <Image
                          style={{
                            width: "80%",
                            height: 60,
                            resizeMode: "cover"
                          }}
                          source={{
                            uri: `http://206.189.159.149:8080/abc/${
                              this.state.bottomBanner
                            }`
                          }}
                        />
                      </View>
                    </View>
                  ) : (
                    <TouchableOpacity
                      onPress={() =>
                        this.navigatetonewsdetails(topUpdate.news.id)
                      }
                    >
                      <View style={styles.carditem}>
                        <Image
                          style={styles.cardImage}
                          source={{
                            uri: `http://206.189.159.149:8080/abc/${
                              topUpdate.news.featruedImage.url
                            }`
                          }}
                        >
                          <View style={styles.cardContent}>
                            <Text style={styles.newsCategory}>CATEGORY</Text>
                            <Text style={styles.newsTitle}>
                              {topUpdate.news.title}
                            </Text>
                            <Text style={styles.newsTime}>
                              {topUpdate.news.createdAt}
                            </Text>
                          </View>
                        </Image>
                      </View>
                    </TouchableOpacity>
                  )}
                </View>
              ) : (
                <View style={{ height: 200 }}>
                  <WebView
                    style={styles.WebViewContainer}
                    javaScriptEnabled
                    domStorageEnabled
                    source={{
                      uri: `https://www.youtube.com/embed/${
                        topUpdate.video.url
                      }`
                    }}
                  />
                </View>
              )
          )}
          <View
            style={{
              width: "100%",
              backgroundColor: "white",
              // flex: 1,
              paddingBottom: 2,
              paddingTop: 2,
              // paddingLeft: 15,
              // paddingRight: 15,
              flexDirection: "row",
              justifyContent: "center"
            }}
          >
            <Image
              style={{ width: "80%", height: 60, resizeMode: "cover" }}
              source={{
                uri: `http://206.189.159.149:8080/abc/${
                  this.state.bottomBanner
                }`
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}
