// import React, { Component } from "react";
// import { Image, StyleSheet, Text, View } from "react-native";

// const styles = StyleSheet.create({
//   swiperbox: {
//     flex: 1.17,
//     height: 245,
//     width: "100%",
//     backgroundColor: "white",
//     marginBottom: 4
//   },
//   swiperbody: { flex: 1 },
//   swiperimagecontainer: { flex: 1 },
//   swiperslidecontainer: { flex: 1 },
//   swiperdottraydotcontainer: {},
//   swiperdottray: {},
//   swiperdot: {},
//   slide: { flex: 1 },
//   slidestrech: { flex: 1 },
//   labelview: {
//     flexDirection: "row",
//     alignItems: "flex-end",
//     justifyContent: "space-between",
//     padding: 5,
//     backgroundColor: "#ffffff"
//   },

//   seeAll: {
//     color: "#36be6b",
//     flexDirection: "column",
//     paddingRight: 10,
//     fontFamily: "Roboto-Medium"
//   },
//   lableviewtittle: {
//     // fontSize :16,
//     fontFamily: "Roboto-Medium",
//     // fontWeight="bold",
//     //  Color="#4a4a4a",
//     fontSize: 16,
//     paddingLeft: 12,
//     paddingTop: 4
//   },
//   flagright: {
//     flex: 1,
//     paddingTop: 27,
//     paddingLeft: 16,
//     //  paddingBottom:5,
//     borderBottomColor: "#36be6b",
//     borderBottomWidth: 5,
//     flexDirection: "row-reverse",
//     margin: 0
//     // backgroundColor:'grey'
//   },
//   logobetweencountries: {
//     // flexDirection:'column',
//     position: "relative"
//   },
//   flagleft: {
//     flex: 1,
//     paddingTop: 27,
//     paddingLeft: 16,
//     borderBottomColor: "#36be6b",
//     borderBottomWidth: 5,
//     flexDirection: "row",
//     margin: 0
//   },
//   leftcountryname: {
//     fontSize: 52,
//     paddingLeft: 8,
//     // paddingTop: 10,
//     color: "black",
//     alignItems: "flex-end",
//     fontFamily: "BigNoodleTitling",
//     bottom: 8
//   },
//   rightcountryname: {
//     fontSize: 52,
//     paddingRight: 8,
//     // paddingTop: 10,
//     color: "black",
//     alignItems: "flex-start",
//     fontFamily: "BigNoodleTitling",
//     bottom: 8
//   },
//   scoresleft: {
//     fontSize: 17,
//     paddingLeft: 58,
//     paddingTop: 8,
//     fontFamily: "Montserrat",
//     color: "#2b2b2b"
//   },
//   scoresright: {
//     fontSize: 17,
//     paddingLeft: 0,
//     paddingTop: 8,
//     fontFamily: "Montserrat",
//     color: "#2b2b2b"
//   },
//   countryelement: {
//     width: "36%",
//     height: "90%",
//     flexDirection: "column",
//     padding: 0
//   },
//   slideBackgroundimage: {
//     flex: 1
//   },
//   insideslidetoprow: {
//     flex: 1,
//     flexDirection: "row",
//     justifyContent: "space-between",
//     padding: 0,
//     alignItems: "center"
//   },
//   insideslidebottomrow: {
//     flex: 1,
//     flexDirection: "column",
//     alignItems: "center",
//     padding: 0,
//     top: 6
//   },
//   textinsidebottonrow: {
//     padding: 0,
//     fontFamily: "Roboto",
//     fontSize: 15,
//     justifyContent: "center",
//     margin: 1,
//     lineHeight: 14,
//     color: "#9b9b9b"
//   }
// });
// export default class Swiper extends Component {
//   render() {
//     return (
//       <View>
//             <View style={styles.swiperbox}>

//                 <Swiper
//                     dot={<View style={{
//                 style=
//                 {{
//                   backgroundColor: "white",
//                   width: 8,
//                   height: 8,
//                   borderRadius: 4,
//                   marginLeft: 3,
//                   marginRight: 3,
//                   marginTop: 3,
//                   marginBottom: 3
//                 }}
//                       />}
//                     activeDotColor="#6F6F6F"
//             dotColor="#ffffff"
//             showsButtons={false}
//                   >

//                     <View style={styles.slide1}>
//               <View style={styles.slide}>
//                             {/* <Image style={{

//         flex: 1,
//         position: 'absolute',
//         top: -90,

//       }}
//         source={require('../resources/icons/1.png')}
//       /> */}
//                             <View style={styles.slidestrech}>
//                   <View style={styles.insideslidetoprow}>
//                                     <View style={styles.countryelement}>
//                       <View style={styles.flagleft}>
//                         <Image
//                           style={{ height: 39, width: 61 }}
//                                                 source={require('../resources/icons/bangladesh.jpg')}
//                                               />
//                         <Text style={styles.leftcountryname}>BAN</Text>
//                       </View>
//                                         <Text style={styles.scoresleft} >225/4 (20)</Text>
//                     </View>
//                     <View style={styles.logobetweencountries}>
//                                         <Image
//                         style={{ height: 63, width: 50 }}
//                                             source={require('../resources/icons/series2.png')}
//                                           />
//                                       </View>
//                                     <View style={styles.countryelement} >
//                                         <View style={styles.flagright}>
//                         <Image
//                           style={{ height: 39, width: 61 }}
//                                                 source={require('../resources/icons/india.jpg')}
//                                               />
//                         <Text style={styles.rightcountryname}>IND</Text>
//                       </View>
//                                         <Text style={styles.scoresright} >141/0 (18.3)</Text>
//                     </View>
//                                   </View>
//                   <View style={styles.insideslidebottomrow}>
//                                     <Text style={styles.textinsidebottonrow}>T20 2 of 2</Text>
//                                     <Text style={{
//                         fontSize: 19,
//                         fontFamily: "Roboto",
//                         fontWeight: "bold",
//                         color: "#000000"
//                       }}
//                                       >Bangladesh won series 2-0
//                     </Text>
//                     {/* fontSize: 19, fontcolor: 'Black' */}
//                                     <Text style={styles.textinsidebottonrow}>Senwes Park,Potchefstroom</Text>
//                                   </View>
//                               </View>
//               </View>
//             </View>
//           </Swiper>
//         </View>
//       </View>
//     );
//   }
// }
