import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  Image,
  ListView,
  StyleSheet,
  View,
  AsyncStorage,
  Alert,
  TouchableOpacity
} from "react-native";
import axios from "axios";
import InfiniteScroll from "react-native-infinite-scroll";
import { getuserdetails } from "../components/authorizationandrefreshtoken";
import Test from "../resources/img/test.png";

const styles = StyleSheet.create({
  container: {
    // paddingBottom: 60,
  },
  carditem: {
    width: "100%",
    backgroundColor: "white",
    //flex: 1,
    paddingBottom: 2,
    paddingTop: 2,
    // paddingLeft: 15,
    // paddingRight: 15,
    justifyContent: "space-around"
  },
  cardImage: {
    height: 200,
    width: "100%"
  },
  cardImageVideos: {
    height: 100,
    width: 150
  },
  newsCategory: {
    backgroundColor: "#169757",
    color: "white",
    width: 110,
    // borderRadius: 20,
    fontFamily: "Roboto",
    fontSize: 12,
    textAlign: "center"
  },
  newsTime: {
    paddingTop: 5,
    // color: 'white',
    fontFamily: "roboto",
    fontSize: 12,
    fontWeight: "normal",
    textAlign: "left",
    color: "rgba(255, 255, 255, 0.87)"
  },
  cardContent: {
    height: "100%",
    alignItems: "flex-end"
    //paddingTop: "51%"
  },

  exploremore: {
    width: "100%",
    backgroundColor: "white",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderTopColor: "#ffffff",
    borderBottomColor: "#ffffff"
  },
  textandvideolength: {
    flexDirection: "column",
    flexWrap: "wrap"
  },
  NewsTitleView: {
    height: 60,
    width: "90%",
    flexDirection: "row"
  },
  NewsTitleLogo: {
    marginTop: 10,
    marginLeft: 25,
    height: 40,
    width: "10%"
  },
  NewsTitleText: {
    marginLeft: 5,
    marginTop: 10,
    color: "#4a4a4a",
    fontFamily: "roboto",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal"
  },
  Banner: {
    height: 50,
    width: "100%"
  }
});

export default class Update extends Component {
  static propTypes = {
    nav: PropTypes.object.isRequired
  };
  // pagination_no = 0;
  constructor(props) {
    super(props);
    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      pageNo: 0,
      access_token: "",
      loadingstate: true,
      newsObjects: [],
      loading: true,
      topBanner: ""
    };
  }

  componentDidMount() {
    AsyncStorage.getItem("user")
      .then(response => {
        //alert(JSON.stringify(response))
        const x = JSON.parse(response);
        axios
        .get(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/banner/specific?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret&pageNumber=7&position=1`
        )
        .then(banner => {
          console.log(banner);
          this.setState({
            topBanner: banner.data.image.url
          });
        });
        this.setState(
          {
            access_token: x.access_token
          },
          () => {
            this.axiosGetVideoContents(
              `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/news/page?access_token=${
                x.access_token
              }&client_id=android-client&client_secret=android-secret&id=0`
            );
          }
        );
      })
      .catch(() => {
        Alert.alert(
          "Cannot connect to internal storage, make sure you have the correct storage rights."
        );
      });
  }
  loadMorePage = () => {
    this.setState({
      loading: false
    });
    this.setState({
      pageNo: this.state.pageNo + 1
    });
    this.axiosGetVideoContents(
      `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/news/page?access_token=${
        this.state.access_token
      }&client_id=android-client&client_secret=android-secret&id=${
        this.state.pageNo
      }`
    );
  };

  navigatetonewsdetails = id => {
    this.props.nav.navigate("NewsDetails", { newsid: id });
  };

  axiosGetVideoContents = async urlvariable => {
    axios
      .get(urlvariable)
      .then(response => {
        this.setState({
          loading: false
        });
        //console.log(response.data.content[0].featruedImage.url);
        response.data.content.map(news => {
          this.setState({
            newsObjects: [...this.state.newsObjects, news]
          });
          return "";
        });
        // alert(JSON.stringify(this.state.videoObjects));
      })
      .catch(error => {
        //console.log(error);
        if (error.response.status === 401) {
          getuserdetails()
            .then(res => {
              this.setState(
                {
                  access_token: res.access_token
                },
                () => {
                  this.axiosGetVideoContents(
                    `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/news/page?access_token=${
                      this.state.access_token
                    }&client_id=android-client&client_secret=android-secret&id=${
                      this.state.pageNo
                    }`
                  );
                }
              );
            })
            .catch(() => {
              this.setState({
                loading: false
              });
              Alert.alert(
                "you are being logged out for unavilability, Please log in again!"
              );
              this.props.nav.navigate("LoginPage");
            });
        } else {
          this.setState({
            loading: false
          });
        }
      });
  };

  render() {
    const rows = this.dataSource.cloneWithRows(this.state.newsObjects || []);
    return (
      <View style={styles.container}>
        {this.state.topBanner !== "" ? (
          <View
            style={{
              width: "100%",
              backgroundColor: "white",
              // flex: 1,
              paddingBottom: 2,
              paddingTop: 2,
              // paddingLeft: 15,
              // paddingRight: 15,
              flexDirection: "row",
              justifyContent: "center"
            }}
          >
            <Image
              style={{ width: "80%", height: 60, resizeMode: "cover" }}
              source={{
                uri: `http://206.189.159.149:8080/abc/${this.state.topBanner}`
              }}
            />
          </View>
        ) : null}
        <InfiniteScroll
          horizontal={false} //true - if you want in horizontal
          onLoadMoreAsync={this.loadMorePage}
          distanceFromEnd={1}
        >
          {this.state.loadingstate === true ||
          this.state.loadingstate === false ? (
            <View style={styles.news}>
              <ListView
                enableEmptySections
                dataSource={rows}
                renderRow={data => (
                  <View>
                    <TouchableOpacity
                      onPress={() => this.navigatetonewsdetails(data.id)}
                    >
                      <View style={styles.carditem}>
                        <Image
                          style={styles.cardImage}
                          source={{
                            uri: `http://206.189.159.149:8080/abc/${
                              data.featruedImage.url
                            }`
                          }}
                        >
                          <View style={styles.cardContent}>
                            <Text style={styles.newsCategory}>
                              {data.createdAt}
                            </Text>
                          </View>
                        </Image>
                      </View>
                    </TouchableOpacity>
                    <View style={styles.NewsTitleView}>
                      <Image style={styles.NewsTitleLogo} source={Test} />
                      <Text style={styles.NewsTitleText}>{data.title}</Text>
                    </View>
                  </View>
                )}
              />
              {/* <View>
                <Image style={styles.Banner} source={Test} />
              </View> */}
            </View>
          ) : (
            <Text>Loading</Text>
          )}
        </InfiniteScroll>
      </View>
    );
  }
}
