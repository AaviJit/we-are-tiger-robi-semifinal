import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  BackHandler
} from "react-native";
import robiShop from "../resources/icons/robiShop.png";
import Logo from "../resources/icons/bitmap.png";
import HomeIcon from "../resources/icons/home_side_menu.png";
import MatchIcon from "../resources/icons/match_side_menu.png";
import NewsIcon from "../resources/icons/news_side_menu.png";
import TigerIcon from "../resources/icons/tiger_side_menu.png";
import SupporterIcon from "../resources/icons/supporter_side_menu.png";
//import CartIcon from "../resources/icons/cart.png";
import Icon from "react-native-vector-icons/Ionicons";
import ProfileIcon from "../resources/icons/profile.png";
//import RobiShopIcon from "../resources/icons/robi_shop.png";
import WeAreTigers from "../resources/icons/we_are_tigers_side_menu.png";

const styles = StyleSheet.create({
  topLogo: {
    width: "100%",
    flexDirection: "column",
    height: "26%",
    backgroundColor: "#36be6e",
    alignItems: "center",
    justifyContent: "center"
  },
  midList: {
    width: "100%",
    height: "34%"
  },
  bottomList: {
    width: "100%",
    height: "34%"
  },
  padview: {
    paddingTop: "10%"
  },
  textsize: {},
  listContianer: {
    width: "100%",
    height: 42,
    flexDirection: "row",
    justifyContent: "flex-start",
    borderBottomWidth: 0.5,
    borderBottomColor: "white",
    borderColor: "white"
    // backgroundColor: 'red',
  },
  listTexts: {
    fontSize: 14,
    color: "white",
    fontFamily: "Roboto",
    fontWeight: "normal",
    fontStyle: "normal",
    // lineHeight: 14.5,
    letterSpacing: 0,
    textAlign: "left",
    width: "80%"
    // backgroundColor: 'grey'
  },
  listIcons: {
    marginRight: 6,
    height: 20,
    width: 20
  },
  listIconContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    paddingRight: 6,
    height: "100%",
    width: "20%"
    // backgroundColor:'red',
  },
  listTextContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    // backgroundColor:'grey',
    width: "80%",
    height: "100%"
  }
});
export default class sidebar extends Component {
  static propTypes = {
    nav: PropTypes.object.isRequired
  };

  toHome = () => {
    this.clearSetintv();
    this.props.nav.navigate("HomePage");
  };
  toMatches = () => {
    this.clearSetintv();
    this.props.nav.navigate("AllMatches");
  };
  toNews = () => {
    this.clearSetintv();
    // console.log(this.props.nav);

    this.props.nav.navigate("NewsAndVideos");
  };
  toTigers = () => {
    this.clearSetintv();
    this.props.nav.navigate("TigersTerritory");
    //this.props.nav.navigate("TigersTerritory");
  };
  toFanZone = () => {
    this.clearSetintv();
    this.props.nav.navigate("FanZone");
    //this.props.nav.navigate("");
  };
  toTigerShop = () => {
    this.clearSetintv();
    this.props.nav.navigate("RobiShop");
  };
  toFanZone = () => {
    this.clearSetintv();
    this.props.nav.navigate("FanZone");
  };
  toLogin = () => {
    this.clearSetintv();
    AsyncStorage.clear();
    this.props.nav.navigate("LogOut");
  };
  toSettings = () => {
    this.clearSetintv();
    this.props.nav.navigate("Settings");
    //Alert.alert("Hey there!", "This feature is yet to come!");
  };
  toProfile = () => {
    this.clearSetintv();
    this.props.nav.navigate("UserProfile");
    //Alert.alert("Hey there!", "This feature is yet to come!");
  };
  toTigers = () => {
    this.clearSetintv();
    this.props.nav.navigate("TigersTerritory");
  };
  clearSetintv = () => {
    console.log("clearing...");
    for (let i = 1; i < 99999; i++) {
      clearInterval(i);
    }
  };
  render() {
    return (
      <View style={{ backgroundColor: "#36be6b", flex: 1 }}>
        <View style={styles.topLogo}>
          <Image style={{ height: 70, width: 70, bottom: 12 }} source={Logo} />
        </View>
        <View style={styles.midList}>
          <View
            style={{ width: "100%", height: 1, backgroundColor: "#50be7b" }}
          />
          <TouchableOpacity onPress={this.toHome}>
            <View style={styles.listContianer}>
              <View style={styles.listIconContainer}>
                <Image style={styles.listIcons} source={HomeIcon} />
              </View>
              <View style={styles.listTextContainer}>
                <Text style={styles.listTexts}>Home</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View
            style={{ width: "100%", height: 1, backgroundColor: "#50be7b" }}
          />
          <TouchableOpacity onPress={this.toMatches}>
            <View style={styles.listContianer}>
              <View style={styles.listIconContainer}>
                <Image style={styles.listIcons} source={MatchIcon} />
              </View>
              <View style={styles.listTextContainer}>
                <Text style={styles.listTexts}>Matches</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View
            style={{ width: "100%", height: 1, backgroundColor: "#50be7b" }}
          />
          <TouchableOpacity onPress={this.toNews}>
            <View style={styles.listContianer}>
              <View style={styles.listIconContainer}>
                <Image style={styles.listIcons} source={NewsIcon} />
              </View>
              <View style={styles.listTextContainer}>
                <Text style={styles.listTexts}>News</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View
            style={{ width: "100%", height: 1, backgroundColor: "#50be7b" }}
          />
          <TouchableOpacity onPress={this.toTigers}>
            <View style={styles.listContianer}>
              <View style={styles.listIconContainer}>
                <Image style={styles.listIcons} source={TigerIcon} />
              </View>
              <View style={styles.listTextContainer}>
                <Text style={styles.listTexts}>Tiger&apos;s Territory</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View
            style={{ width: "100%", height: 1, backgroundColor: "#50be7b" }}
          />
          <TouchableOpacity onPress={this.toFanZone}>
            <View style={styles.listContianer}>
              <View style={styles.listIconContainer}>
                <Image style={styles.listIcons} source={SupporterIcon} />
              </View>
              <View style={styles.listTextContainer}>
                <Text style={styles.listTexts}>Fan Zone</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View
            style={{ width: "100%", height: 1, backgroundColor: "#50be7b" }}
          />
        </View>
        <View style={styles.padview} />
        <View style={styles.bottomList}>
          <TouchableOpacity onPress={this.toProfile}>
            <View style={styles.listContianer}>
              <View style={styles.listIconContainer}>
                <Image
                  style={{
                    marginRight: 6,
                    height: 20,
                    width: 17
                  }}
                  source={ProfileIcon}
                />
              </View>
              <View style={styles.listTextContainer}>
                <Text style={styles.listTexts}>Profile</Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.toSettings}>
            <View style={styles.listContianer}>
              <View style={styles.listIconContainer}>
                <Icon
                  name="md-settings"
                  size={25}
                  color="white"
                  style={{
                    marginRight: 6,
                    height: 25,
                    width: 20
                  }}
                />
              </View>
              <View style={styles.listTextContainer}>
                <Text style={styles.listTexts}>Settings</Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.toTigerShop}>
            <View style={styles.listContianer}>
              <View style={styles.listIconContainer}>
                <Image
                  style={{
                    marginRight: 6,
                    height: 20,
                    width: 20
                  }}
                  source={robiShop}
                />
              </View>
              <View style={styles.listTextContainer}>
                <Text style={styles.listTexts}>Robi Shop</Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.toLogin}>
            <View style={styles.listContianer}>
              <View style={styles.listIconContainer}>
                <Image style={styles.listIcons} source={WeAreTigers} />
              </View>
              <View style={styles.listTextContainer}>
                <Text style={styles.listTexts}>Log out</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
