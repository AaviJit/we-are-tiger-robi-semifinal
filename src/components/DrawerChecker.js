let DrawarOpen = false;

export function DrawerChecker() {
  return DrawarOpen;
}

export function DrawerChanger(change, drawer) {
  DrawarOpen = change;
  if (drawer) {
    this.drawer = drawer;
  }
  if (change) {
    this.drawer._root.open();
  } else {
    this.drawer._root.close();
  }
}
