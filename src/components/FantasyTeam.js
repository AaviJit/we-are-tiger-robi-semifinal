import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  StyleSheet
} from "react-native";
import Image1 from "../components/Images/backgroundImage.png";

// import PropTypes from 'prop-types';
const styles = StyleSheet.create({
  Text1: {
    fontSize: 50,
    fontFamily: "Roboto",
    fontWeight: "bold",
    color: "#ffffff",
    marginTop: "40%",
    textShadowColor: "rgba(0, 0, 0, 0.85)",
    textShadowOffset: {
      width: 0,
      height: 0.5
    },
    textShadowRadius: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    paddingLeft: 20,
    paddingRight: 20,
    lineHeight: 42,
    width: "80%"
  },
  Image1View: {
    borderWidth: 1,
    borderColor: "#ffffff",
    borderRadius: 100,
    backgroundColor: "#FF0000",
    justifyContent: "center",
    alignItems: "center",
    height: 35,
    width: 250,
    marginTop: 70,
    marginRight: 20,
    marginLeft: 60
  },
  Image1Text: {
    justifyContent: "center",
    alignItems: "center",
    fontSize: 13,
    fontFamily: "Roboto",
    fontWeight: "normal",
    fontStyle: "normal",
    textAlign: "center",
    color: "rgba(255, 255, 255, 0.87)"
  }
});
const FantasyTeam = () => (
  <View style={{ flex: 1 }}>
    <ScrollView>
      <View style={{ flex: 1 }}>
        <Image source={Image1}>
          <Text style={styles.Text1}>MAKE YOUR OWN TEAM</Text>
          <TouchableOpacity>
            <View style={styles.Image1View}>
              <Text style={styles.Image1Text}>ENTER THE CHALLENGE</Text>
            </View>
          </TouchableOpacity>
        </Image>
      </View>
    </ScrollView>
  </View>
);
export default FantasyTeam;
