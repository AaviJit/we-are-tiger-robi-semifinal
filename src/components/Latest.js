import React from "react";
import {
  View,
  StyleSheet,
  FlatList,
  ScrollView,
  Image,
  Text,
  Dimensions
} from "react-native";
import TestPic from "../resources/img/test.png";

const styles = StyleSheet.create({
  list: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginLeft: 1,
    marginRight: 1
  }
});

const Latest = () => {
  const renderItem = () => (
    <View
      style={{
        margin: 10,
        width: (Dimensions.get("window").width - 42) / 2,
        height: ((Dimensions.get("window").width - 22) / 2) * 1.4,
        backgroundColor: "#ffffff"
      }}
    >
      <Image
        style={{ flex: 1, height: "95%", width: "100%" }}
        source={TestPic}
      />
      <Text
        style={{
          fontSize: 15,
          fontFamily: "Lato",
          fontWeight: "bold",
          color: "#171616"
        }}
      >
        Marigson margarite
      </Text>
      <Text style={{ fontSize: 10, fontFamily: "Lato", color: "#171616" }}>
        290$
      </Text>
    </View>
  );
  return (
    <ScrollView>
      <View style={{ flex: 1, marginTop: 8, paddingBottom: 50 }}>
        <FlatList
          contentContainerStyle={styles.list}
          data={[{ key: "1" }, { key: "2" }, { key: "3" }, { key: "4" }]}
          renderItem={renderItem}
        />
      </View>
    </ScrollView>
  );
};
export default Latest;
