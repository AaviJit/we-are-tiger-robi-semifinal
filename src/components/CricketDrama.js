import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  ListView,
  ScrollView,
  StyleSheet,
  View,
  AsyncStorage,
  Alert
} from "react-native";
import axios from "axios";
import InfiniteScroll from "react-native-infinite-scroll";
import Thumbnail from "react-native-thumbnail-video";
import { getuserdetails } from "../components/authorizationandrefreshtoken";

const styles = StyleSheet.create({
  container: {
    // paddingBottom: 60,
  },
  carditem: {
    width: "100%",
    backgroundColor: "white",
    // flex: 1,
    paddingBottom: 2,
    paddingTop: 2,
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  cardImage: {
    height: 200,
    width: "100%"
  },
  cardImageVideos: {
    height: 100,
    width: 150
  },
  exploremore: {
    width: "100%",
    backgroundColor: "white",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderTopColor: "#ffffff",
    borderBottomColor: "#ffffff"
  },
  textandvideolength: {
    flexDirection: "column",
    flexWrap: "wrap"
  }
});

export default class CricketDrama extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  // pagination_no = 0;
  constructor(props) {
    super(props);
    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      pageNo: 0,
      access_token: "",
      loadingstate: true,
      videoObjects: []
    };
  }

  componentDidMount() {
    AsyncStorage.getItem("user")
      .then(response => {
        //alert(JSON.stringify(response))
        const x = JSON.parse(response);
        this.setState(
          {
            access_token: x.access_token
          },
          () => {
            this.axiosGetVideoContents(
              `http://159.89.195.154:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/video/page?access_token=${
                x.access_token
              }&client_id=android-client&client_secret=android-secret&id=0`
            );
          }
        );
      })
      .catch(() => {});
  }
  loadMorePage = () => {
    this.setState({
      pageNo: this.state.pageNo + 1
    });
    this.axiosGetVideoContents(
      `http://159.89.195.154:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/video/page?access_token=${
        this.state.access_token
      }&client_id=android-client&client_secret=android-secret&id=${
        this.state.pageNo
      }`
    );
  };

  axiosGetVideoContents = async urlvariable => {
    axios
      .get(urlvariable)
      .then(response => {
        response.data.map(
          video => {
            this.setState({
              videoObjects: [...this.state.videoObjects, video]
            });
            return null;
          },
          () => {
            this.setState({
              loadingstate: false
            });
          }
        );
        // alert(JSON.stringify(this.state.videoObjects));
      })
      .catch(error => {
        if (error.response.status === 401) {
          getuserdetails()
            .then(res => {
              this.axiosGetVideoContents(
                `http://159.89.195.154:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/video/page?access_token=${
                  res.access_token
                }&client_id=android-client&client_secret=android-secret&id=${
                  this.state.pageNo
                }`
              );
            })
            .catch(() => {
              Alert.alert(
                "you are being logged out for unavilability, Please log in again!"
              );
              this.props.navigation.navigate("LoginPage");
            });
        } else {
          //alert(error.response.status);
          //Alert.alert("Sorry, no More videos to display!");
        }
      });
  };

  render() {
    const rows = this.dataSource.cloneWithRows(this.state.videoObjects || []);
    return (
      <ScrollView>
        <View style={styles.container}>
          <InfiniteScroll
            horizontal={false} //true - if you want in horizontal
            onLoadMoreAsync={this.loadMorePage}
            distanceFromEnd={1}
          >
            {this.state.loadingstate === true ||
            this.state.loadingstate === false ? (
              <View style={styles.news}>
                <ListView
                  enableEmptySections
                  dataSource={rows}
                  renderRow={data => (
                    <View style={styles.carditem}>
                      <Thumbnail url={data.url} />
                    </View>
                  )}
                />
              </View>
            ) : (
              <Text>Loading</Text>
            )}
          </InfiniteScroll>
        </View>
      </ScrollView>
    );
  }
}
