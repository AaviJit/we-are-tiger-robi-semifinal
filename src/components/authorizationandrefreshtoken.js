import { AsyncStorage } from "react-native";
import axios from "axios";

const axiosCall = x =>
  new Promise((resolve, reject) => {
    axios
      .get(
        `http://159.89.195.154:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/oauth/token?refresh_token=${x}&grant_type=refresh_token&client_id=android-client&client_secret=android-secret`
      )
      .then(response => {
        //saving the new information to asyncstorage
        AsyncStorage.setItem("user", JSON.stringify(response.data));
        resolve(response.data);
      })
      .catch(error => {
        reject(error);
      })
      .done();
  });

export const getUserdetails = () =>
  new Promise((resolve, reject) => {
    AsyncStorage.getItem("user")
      .then(response => {
        const x = JSON.parse(response);
        axiosCall(x.refresh_token)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            //console.log(error.error_description);
            reject(error);
          });
      })
      .catch(error => {
        reject(error);
      })
      .done();
  });

export const hellohellodinosaur = () => "hell yeah";
