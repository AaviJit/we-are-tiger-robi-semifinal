import { Animated, Easing } from "react-native";
import { StackNavigator, NavigationActions } from "react-navigation";

import LoginPage from "./containers/LoginPage";
import HomePage from "./containers/HomePage";
import RegisterPage from "./containers/RegisterPage";
import MatchDetails from "./containers/MatchDetails";
import FanZone from "./containers/FanZone";
import NewsAndVideos from "./containers/NewsAndVideos";
import NewsDetails from "./containers/NewsDetails";
import PlayersWallpaper from "./containers/PlayersWallpaper";
import ProductCart from "./containers/ProductCart";
import ProductDetails from "./containers/ProductDetails";
import Products from "./containers/Products";
import Settings from "./containers/Settings";
import ShippingInformation from "./containers/ShippingInformation";
import TigersShop from "./containers/TigersShop";
import TigersTerritory from "./containers/TigersTerritory";
import UserProfile from "./containers/UserProfile";
import AllMatches from "./containers/AllMatches";
import RobiShop from "./containers/RobiShop";
import Comment from "./containers/Comment";
import LogOut from "./containers/LogOutPage";
import SliderPage from "./containers/sliderPage";
import UpcomingInfo from "./containers/upComingInfo";

// import { View } from 'native-base';

const AppNavigator = new StackNavigator(
  {
    LoginPage: { screen: LoginPage },
    HomePage: { screen: HomePage },
    RegisterPage: { screen: RegisterPage },
    MatchDetails: { screen: MatchDetails },
    FanZone: { screen: FanZone },
    NewsAndVideos: { screen: NewsAndVideos },
    NewsDetails: { screen: NewsDetails },
    PlayersWallpaper: { screen: PlayersWallpaper },
    ProductCart: { screen: ProductCart },
    ProductDetails: { screen: ProductDetails },
    Products: { screen: Products },
    Settings: { screen: Settings },
    ShippingInformation: { screen: ShippingInformation },
    TigersShop: { screen: TigersShop },
    TigersTerritory: { screen: TigersTerritory },
    UserProfile: { screen: UserProfile },
    AllMatches: { screen: AllMatches },
    RobiShop: { screen: RobiShop },
    Comment: { screen: Comment },
    LogOut: { screen: LogOut },
    SliderPage: { screen: SliderPage },
    UpcomingInfo: { screen: UpcomingInfo },
  },
  {
    headerMode: "screen",
    navigationOptions: {
      header: null
    },
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0,
        timing: Animated.timing,
        easing: Easing.step0
      }
    })
  }
);
NavigationActions.reset({
  index: 0,
  key: null,
  actions: [
    NavigationActions.navigate({ routeName: "HomePage" }),
    NavigationActions.navigate({ routeName: "AllMatches" }),
    NavigationActions.navigate({ routeName: "MatchDetails" }),
    NavigationActions.navigate({ routeName: "NewsAndVideos" }),
    NavigationActions.navigate({ routeName: "NewsDetails" }),
    NavigationActions.navigate({ routeName: "RobiShop" }),
    NavigationActions.navigate({ routeName: "TigersTerritory" })
  ]
});
export default AppNavigator;
